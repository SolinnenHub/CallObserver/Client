package ru.ranepa.zf.callobserver;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import github.nisrulz.qreader.QRDataListener;
import github.nisrulz.qreader.QREader;

import static ru.ranepa.zf.callobserver.App.mSettings;

public class ActivityRegistration extends AppCompatActivity {

    private SurfaceView mySurfaceView;
    private QREader qrEader;
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        MainActivity.requestPermissions(this);

        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message message) {
                Toast.makeText(getApplicationContext(), "Теперь вы можете осуществить регистрацию.", Toast.LENGTH_LONG).show();
            }
        };


        mySurfaceView = findViewById(R.id.camera_view);
        qrEader = new QREader.Builder(this, mySurfaceView, data -> {

            if (!data.equals("")) {
                SharedPreferences.Editor editor = mSettings.edit();
                try {
                    String baseUrl = data.substring(0, data.indexOf("join/?k="));
                    editor.putString("baseUrl", baseUrl);
                    editor.apply();
                } catch (Exception ignored) {}

                Message message = mHandler.obtainMessage(); message.sendToTarget();
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra("registrationUrl", data);
                startActivity(intent);
                finish();
            }

        }).facing(QREader.BACK_CAM)
                .enableAutofocus(true)
                .build();
    }

    @Override
    protected void onResume() {
        super.onResume();
        qrEader.initAndStart(mySurfaceView);
    }

    @Override
    protected void onPause() {
        super.onPause();
        qrEader.releaseAndCleanup();
    }
}
