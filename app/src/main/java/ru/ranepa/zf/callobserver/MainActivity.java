package ru.ranepa.zf.callobserver;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import org.mozilla.geckoview.GeckoRuntime;
import org.mozilla.geckoview.GeckoRuntimeSettings;
import org.mozilla.geckoview.GeckoSession;
import org.mozilla.geckoview.GeckoSessionSettings;
import org.mozilla.geckoview.GeckoView;

import java.util.Objects;

import ru.ranepa.zf.callobserver.gecko.GeckoViewPrompt;
import ru.ranepa.zf.callobserver.gecko.MyProgressDelegate;

import static ru.ranepa.zf.callobserver.App.mSettings;
import static ru.ranepa.zf.callobserver.MyService.geckoRuntime;
import static ru.ranepa.zf.callobserver.MyService.geckoSession;

public class MainActivity extends AppCompatActivity {

    public static boolean isServiceRunning = false;
    public static boolean wasActivitySettingsOpened = false;
    public static Intent serviceIntent;
    private boolean isWebInitialized = false;
    private Toolbar toolbar;
    private GeckoView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        webView = findViewById(R.id.webView);

        requestPermissions(this);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        String url = mSettings.getString("baseUrl", null);
        if (!isWebInitialized) {
            serviceIntent = new Intent(this, MyService.class);
            geckoSession = new GeckoSession();
            geckoSession.getSettings().setAllowJavascript(true);
            geckoSession.getSettings().setDisplayMode(GeckoSessionSettings.DISPLAY_MODE_MINIMAL_UI);
            geckoSession.setProgressDelegate(new MyProgressDelegate());
            geckoSession.setPromptDelegate(new GeckoViewPrompt(this));

            GeckoRuntimeSettings.Builder builder = new GeckoRuntimeSettings.Builder()
                    .javaScriptEnabled(true);

            if (geckoRuntime == null) {
                geckoRuntime = GeckoRuntime.create(this, builder.build());
                geckoRuntime.getSettings().setCookieLifetime(GeckoRuntimeSettings.COOKIE_LIFETIME_DAYS);
            }
            webView.setSession(geckoSession, geckoRuntime);
            isWebInitialized = true;
        }

        if (url == null) {
            Toast.makeText(this, "Осуществите первичную настройку, указав адрес сервера.", Toast.LENGTH_LONG).show();
            startActivity(new Intent(this, ActivityProfile.class));
        } else {
            if (!isServiceRunning) {
                isServiceRunning = true;
                startService(serviceIntent);
            }
            geckoSession.loadUri(Objects.requireNonNull(url));
            webView.setSession(geckoSession, geckoRuntime);
        }
    }

    public static void requestPermissions(Activity activity) {
        String[] PERMISSIONS;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            PERMISSIONS = new String[]{
                    android.Manifest.permission.READ_PHONE_NUMBERS,
                    android.Manifest.permission.CAMERA,
                    android.Manifest.permission.READ_PHONE_STATE,
                    android.Manifest.permission.PROCESS_OUTGOING_CALLS,
                    android.Manifest.permission.RECORD_AUDIO,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.READ_CALL_LOG
            };
        else
            PERMISSIONS = new String[]{
                    android.Manifest.permission.CAMERA,
                    android.Manifest.permission.READ_PHONE_STATE,
                    android.Manifest.permission.PROCESS_OUTGOING_CALLS,
                    android.Manifest.permission.RECORD_AUDIO,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.READ_CALL_LOG
            };
        if(!hasPermissions(activity, PERMISSIONS)) {
            ActivityCompat.requestPermissions(activity, PERMISSIONS, 1);
        }
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onNewIntent(Intent intent) {
        onIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String url = mSettings.getString("baseUrl", null);
        if (url != null && wasActivitySettingsOpened) {
            wasActivitySettingsOpened = false;
            Objects.requireNonNull(webView.getSession()).loadUri(url);

            if (!isServiceRunning) {
                isServiceRunning = true;
                startService(serviceIntent);
            }
        }
        onIntent(getIntent());
    }

    private void onIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            String registrationUrl = extras.getString("registrationUrl");
            String modalUrl = extras.getString("modalUrl");
            if (registrationUrl != null && !registrationUrl.equals("")) {
                Objects.requireNonNull(webView.getSession()).loadUri(registrationUrl);
                extras.putString("registrationUrl", "");
            } else if (modalUrl != null && !modalUrl.equals("")) {
                String baseUrl = mSettings.getString("baseUrl", null);
                String url = baseUrl + "clients?modal=" + modalUrl;
                Objects.requireNonNull(webView.getSession()).loadUri(url);
                extras.putString("modalUrl", "");
            }
        }
    }

    public void stopTracking(MenuItem item) {
        serviceIntent = new Intent(this, MyService.class);
        stopService(serviceIntent);
        MainActivity.isServiceRunning = false;
        finishAndRemoveTask();
        int pid = android.os.Process.myPid();
        android.os.Process.killProcess(pid);
    }

    public void openSettings(MenuItem item) {
        startActivity(new Intent(this, ActivityProfile.class));
    }

    public void refreshWebView(MenuItem item) {
        if (webView.getSession() != null)
            webView.getSession().reload();
    }

    @Override
    public void onBackPressed() {
        geckoSession.goBack();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (toolbar != null)
            toolbar.dismissPopupMenus();
    }
}
