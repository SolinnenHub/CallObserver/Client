package ru.ranepa.zf.callobserver;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;

import com.aykuttasil.callrecord.CallRecord;

import org.mozilla.geckoview.GeckoRuntime;
import org.mozilla.geckoview.GeckoSession;

import static ru.ranepa.zf.callobserver.App.CHANNEL_ID;
import static ru.ranepa.zf.callobserver.App.mSettings;

public class MyService extends Service {

    CallsBroadcastReceiver receiver = new CallsBroadcastReceiver();
    public static GeckoSession geckoSession;
    public static GeckoRuntime geckoRuntime;
    public static CallRecord callRecord;

    @Override
    public void onCreate() {
        super.onCreate();

        callRecord = new CallRecord.Builder(this)
                .setRecordFileName("test")
                .setRecordDirName("Android/data/ru.ranepa.zf.callobserver/files/temp")
                .setRecordDirPath(Environment.getExternalStorageDirectory().getPath())
                .setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
                .setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION)
                .setShowSeed(false)
                .build();
        callRecord.enableSaveFile();
        callRecord.startCallReceiver();

        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_NEW_OUTGOING_CALL);
        filter.addAction(TelephonyManager.EXTRA_STATE_RINGING);
        filter.addAction(TelephonyManager.ACTION_PHONE_STATE_CHANGED);
        registerReceiver(receiver, filter);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String modalUrl = intent.getStringExtra("modalUrl");
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.putExtra("modalUrl", modalUrl);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0,
                notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_phone_solid)
                .setVibrate(new long[]{0L})
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setContentTitle(mSettings.getString("n_title", ""))
                .setContentText(mSettings.getString("n_text", ""))
                .setContentInfo(mSettings.getString("n_tag", ""))
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentIntent(pendingIntent);

        if (intent.getBooleanExtra("highPriority", false)) {
            builder.setPriority(Notification.PRIORITY_HIGH);
        }

        Notification notification = builder.build();

        startForeground(1, notification);
        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
        callRecord.stopCallReceiver();
        MainActivity.isServiceRunning = false;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
