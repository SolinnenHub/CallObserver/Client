package ru.ranepa.zf.callobserver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.android.exoplayer2.util.Util.closeQuietly;
import static org.mozilla.gecko.GeckoAppShell.getApplicationContext;
import static ru.ranepa.zf.callobserver.App.mSettings;
import static ru.ranepa.zf.callobserver.MainActivity.serviceIntent;
import static ru.ranepa.zf.callobserver.MyService.callRecord;

public class CallsBroadcastReceiver extends BroadcastReceiver {

    private final String TAG = "CallsBroadcastReceiver";
    private static String number = null;
    private static String lastState = TelephonyManager.EXTRA_STATE_IDLE;
    private static int direction = -1;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_NEW_OUTGOING_CALL.equals(intent.getAction())) {
            number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
            direction = 2;
            processCall(context, number, "outgoing");
        } else if ("android.intent.action.PHONE_STATE".equals(intent.getAction())) {
            String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                Log.d(TAG, lastState+"->"+TelephonyManager.EXTRA_STATE_RINGING);
                lastState = TelephonyManager.EXTRA_STATE_RINGING;
                number = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
                if (number != null) {
                    direction = 1;
                    processCall(context, number, "incoming");
                }
            }
            if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                Log.d(TAG, lastState+"->"+TelephonyManager.EXTRA_STATE_OFFHOOK);
                if (lastState.equals(TelephonyManager.EXTRA_STATE_RINGING) || lastState.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                    processCall(context, number, "offhook");
                }
                lastState = TelephonyManager.EXTRA_STATE_OFFHOOK;
            }
            if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                if (number != null && lastState.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                    Log.d(TAG, lastState+"->"+TelephonyManager.EXTRA_STATE_IDLE);
                    lastState = TelephonyManager.EXTRA_STATE_IDLE;
                    processCall(context, number, "ended");
                }
            }
        }
    }

    private void processCall(Context ctx, String rawNumber, String mode) {
        StringBuilder builder = new StringBuilder(rawNumber);
        if (builder.charAt(0) == '8')
            builder.replace(0,1,"+7");
        String number = builder.toString();

        if (mode.equals("incoming")) {
            getClientByNumber(ctx, true, false, number, rawNumber);
        }
        if (mode.equals("outgoing")) {
            getClientByNumber(ctx, true, false, number, rawNumber);
        }
        if (mode.equals("offhook")) {

        }
        if (mode.equals("ended")) {
            getClientByNumber(ctx, false, true, number, rawNumber);
        }
    }

    private void deleteFile(Context ctx, String path) {
        File file = new File(path);
        boolean deleted = file.delete();
        if (!deleted)
            Toast.makeText(ctx, "Ошибка удаления локальной записи разговора. Возможно, приложение не получило необходимых разрешений.", Toast.LENGTH_SHORT).show();
    }

    private String readFileAsBase64String(String path) {
        try {
            InputStream is = new FileInputStream(path);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Base64OutputStream b64os = new Base64OutputStream(baos, Base64.DEFAULT);
            byte[] buffer = new byte[8192];
            int bytesRead;
            try {
                while ((bytesRead = is.read(buffer)) > -1) {
                    b64os.write(buffer, 0, bytesRead);
                }
                return baos.toString();
            } catch (IOException e) {
                Log.e(TAG, "Cannot read file " + path, e);
                return "";
            } finally {
                closeQuietly(is);
                closeQuietly(b64os);
            }
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File not found " + path, e);
            return "";
        }
    }

    private List<String> searchFiles(String path, String pattern, String rawNumber) {
        File f = new File(path);
        File[] files=f.listFiles();
        List tFileList = new ArrayList();
        for (File file : files) {
            String filePath = String.valueOf(file.getPath());
            if (filePath.contains(pattern) && filePath.contains(rawNumber)) {
                tFileList.add(filePath);
            }
        }
        return tFileList;
    }

    private void getClientByNumber(Context ctx, boolean pop, boolean addCall, String number, String rawNumber) {
        RequestQueue queue = Volley.newRequestQueue(ctx);
        String baseUrl = mSettings.getString("baseUrl", "");
        String sessionId = mSettings.getString("sessionId", "");
        String url = baseUrl+"clients/getByNumber/"+number+"/";
        StringRequest getRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        JSONObject r = new JSONObject(response);
                        int clientId = r.getInt("id");
                        String name = r.getString("name");
                        String err = r.getString("err");

                        if (err.equals("")) {
                            if (addCall)
                                addCall(ctx, clientId, direction, rawNumber);
                            updateNotification(ctx, pop,
                                    "✆ "+number,
                                    ""+name,
                                    "",
                                    "/clients/client/"+clientId+"/edit/"
                            );
                        } else if (err.equals("NO_SUCH_CLIENT")) {
                            addClient(ctx, number, rawNumber);
                            updateNotification(ctx, pop,
                                    "✆ "+number,
                                    "Нет информации.",
                                    "",
                                    ""
                            );
                        }
                    } catch (JSONException e) {
                        Toast.makeText(ctx, "Ошибка: невозможно прочесть запрос со стороны сервера.", Toast.LENGTH_LONG).show();
                    }
                },
                error -> Toast.makeText(ctx, "Ошибка: "+error, Toast.LENGTH_LONG).show()
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<>();
                params.put("Cookie", "sessionid=" + sessionId);
                return params;
            }
        };
        queue.add(getRequest);
    }

    private void addClient(Context ctx, String number, String rawNumber) {
        RequestQueue queue = Volley.newRequestQueue(ctx);
        String baseUrl = mSettings.getString("baseUrl", "");
        String sessionId = mSettings.getString("sessionId", "");
        String url = baseUrl+"clients/client/";
        StringRequest getRequest = new StringRequest(Request.Method.POST, url,
                response -> getClientByNumber(ctx, false, false, number, rawNumber),
                error -> Toast.makeText(ctx, "Ошибка: "+error, Toast.LENGTH_LONG).show()
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<>();
                params.put("Cookie", "sessionid=" + sessionId);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> mParams;
                mParams = new HashMap<>();
                mParams.put("number", number);
                return mParams;
            }
        };
        queue.add(getRequest);
    }

    private void addCall(Context ctx, int clientId, int modeId, String rawNumber) {
        RequestQueue queue = Volley.newRequestQueue(ctx);
        String baseUrl = mSettings.getString("baseUrl", "");
        String sessionId = mSettings.getString("sessionId", "");
        String url = baseUrl+"calls/call/";
        StringRequest getRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    String path = callRecord.getRecordDirPath()+"/"+callRecord.getRecordDirName()+"/";
                    String pattern = ""+callRecord.getRecordFileName();
                    List<String> files = searchFiles(path, pattern, rawNumber);
                    try {
                        int callId = Integer.parseInt(response.substring(response.indexOf('№')+1));
                        uploadRecord(ctx, callId, files.get(0));
                    } catch (Exception e) {
                        Toast.makeText(ctx, "Ошибка: "+e, Toast.LENGTH_LONG).show();
                    }
                },
                error -> Toast.makeText(ctx, "Ошибка: "+error, Toast.LENGTH_LONG).show()
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<>();
                params.put("Cookie", "sessionid=" + sessionId);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> mParams;
                mParams = new HashMap<>();
                mParams.put("clientId", ""+clientId);
                mParams.put("modeId", ""+modeId);
                return mParams;
            }
        };
        queue.add(getRequest);
    }

    private void uploadRecord(Context ctx, int callId, String path) {
        RequestQueue queue = Volley.newRequestQueue(ctx);
        String baseUrl = mSettings.getString("baseUrl", "");
        String sessionId = mSettings.getString("sessionId", "");
        String url = baseUrl+"calls/call/"+callId+"/uploadRecord/";
        StringRequest getRequest = new StringRequest(Request.Method.POST, url,
                response -> Toast.makeText(ctx, "Запиь звонка загружена на сервер", Toast.LENGTH_LONG).show(),
                error -> Toast.makeText(ctx, "Ошибка: "+error, Toast.LENGTH_LONG).show()
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<>();
                params.put("Cookie", "sessionid=" + sessionId);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                String b64encodedRecord = readFileAsBase64String(path);
                deleteFile(ctx, path);
                Map<String, String> mParams;
                mParams = new HashMap<>();
                mParams.put("b64encodedRecord", b64encodedRecord);
                return mParams;
            }
        };
        queue.add(getRequest);
    }

    private void updateNotification(Context ctx, boolean pop, String title, String text, String tag, String modal) {
        if (text.equals(""))
            tag = "Нажмите для подробностей.";
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("n_title", title);
        editor.putString("n_text", text);
        editor.putString("n_tag", tag);
        editor.apply();

        serviceIntent.putExtra("modalUrl", modal);

        serviceIntent.putExtra("highPriority", false); ctx.startService(serviceIntent);
        if (pop) {
            SystemClock.sleep(2200);
            serviceIntent.putExtra("highPriority", true); ctx.startService(serviceIntent);
        }
    }
}
