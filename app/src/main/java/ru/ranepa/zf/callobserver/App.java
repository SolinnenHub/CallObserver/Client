package ru.ranepa.zf.callobserver;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

public class App extends Application {

    public static final String CHANNEL_ID = "call-observer-channel";
    public static SharedPreferences mSettings;

    @Override
    public void onCreate() {
        super.onCreate();

        mSettings = getSharedPreferences("currentProfile", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("n_title", "Call Observer запущен");
        editor.putString("n_text", "Нажмите для открытия");
        editor.putString("n_tag", "");
        editor.apply();

        createNotificationChannel();
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Name",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }
}
