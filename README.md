# CallObserver client

CallObserver is a CRM for logging calls between customers and employees in your organization. CRM has a mobile application that logs all incoming and outgoing calls with known clients.

Features:

- Client management
- Employee management
- Call management
- Access management
- Call recording (WIP)


Use Android Studio to build this project.

## Demo

![](images/1.jpg)
