package ru.ranepa.zf.callobserver;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Objects;

import static ru.ranepa.zf.callobserver.App.mSettings;
import static ru.ranepa.zf.callobserver.MainActivity.serviceIntent;
import static ru.ranepa.zf.callobserver.MainActivity.wasActivitySettingsOpened;

public class ActivityProfile extends AppCompatActivity {

    private EditText editTextBaseUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        stopService(serviceIntent);
        MainActivity.isServiceRunning = false;

        setTitle(R.string.profile_item);

        Button button = findViewById(R.id.bSettingsSave);
        editTextBaseUrl = findViewById(R.id.editTextBaseUrl);
        EditText editTextSessionId = findViewById(R.id.editTextSessionId);

        editTextBaseUrl.setText(mSettings.getString("baseUrl", ""));
        editTextSessionId.setText(mSettings.getString("sessionId", ""));

        button.setOnClickListener(view1 -> {
            String baseUrl = editTextBaseUrl.getText().toString();
            if (!Objects.equals(baseUrl, "")) {

                if (baseUrl.length() < 9)
                    baseUrl = "http://"+baseUrl;
                else if (!(baseUrl.substring(0, 7).equals("http://") || baseUrl.substring(0, 8).equals("https://"))) {
                    baseUrl = "http://"+baseUrl;
                }
                if (baseUrl.charAt(baseUrl.length()-1) != '/')
                    baseUrl += "/";

                SharedPreferences.Editor editor = mSettings.edit();
                editor.putString("baseUrl", baseUrl);
                editor.apply();

                Toast.makeText(getApplicationContext(), "Успешно сохранено", Toast.LENGTH_SHORT).show();
                wasActivitySettingsOpened = true;
                MainActivity.isServiceRunning = true;
                startService(serviceIntent);
                View view = this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                finish();
            } else {
                editTextBaseUrl.requestFocus();
                Toast.makeText(getApplicationContext(), "Укажите адрес сервера", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            MainActivity.isServiceRunning = true;
            startService(serviceIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MainActivity.isServiceRunning = true;
        startService(serviceIntent);
        moveTaskToBack(false);
    }

    public void openRegistration(View view) {
        startActivity(new Intent(this, ActivityRegistration.class));
    }
}
