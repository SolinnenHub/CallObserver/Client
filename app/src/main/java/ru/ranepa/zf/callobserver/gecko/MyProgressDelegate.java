package ru.ranepa.zf.callobserver.gecko;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import org.mozilla.geckoview.GeckoSession;

import static ru.ranepa.zf.callobserver.App.mSettings;

public class MyProgressDelegate implements GeckoSession.ProgressDelegate {
    @Override
    public void onPageStart(@NonNull GeckoSession session, @NonNull String url) {
        String key = "?sessionId";
        if (url.contains(key)) {
            String value = url.substring(url.indexOf(key)+key.length()+1);
            SharedPreferences.Editor editor = mSettings.edit();
            editor.putString("sessionId", value);
            editor.apply();
        }
    }

    @Override
    public void onPageStop(@NonNull GeckoSession session, boolean success) { }

    @Override
    public void onProgressChange(@NonNull GeckoSession session, int progress) { }

    @Override
    public void onSecurityChange(@NonNull GeckoSession session, @NonNull SecurityInformation securityInfo) { }
}